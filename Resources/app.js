// initalize core

(function() {
	var eduCore = require('modules/core');
	// Setting app defaults.  These are simple properties appended to the app properties object
	eduCore.addProperty('pages', 'ui');
	eduCore.addProperty('plugins','plugins');
	
	/*
	// Include plugins
	Ti.include('plugins/analytics.js'); // analytics library

	// Register plugins with the core app
	eduCore.register('analytics', Analytics); // Register the lib within the app so we can use it elsewhere
	
	eduCore.setupAnalytics('AU-6634319-31');
	*/
	eduCore.loadPage('AppWindow'); 
})();
