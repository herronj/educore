/**
 * Basic Page Module setup
 */


/**
 * private methods & variables
 */


/*********************************
 *  Public Methods & variables
 ********************************/

/**
 * the load function is what the app requires the initialization function of the module to be called
 * all Page modules should be prepped to accept two parans app and Params
 * 
 * app is the context for the hookup to the core app functionality
 * params is any params that the module neeeds passed to it
 * 
 * @param {object} app
 * @param {object} params 
 */
function BasicWindow(app, params) {
	
	//load Dependencies
	var styles = require("ui/styles");

	
	var instance = Titanium.UI.createWindow({
		id : 'BasicWindow',
		title : 'Basic Window',
		backgroundColor : '#ffffff',
		barColor: '#414444',
		navBarHidden : false
	});
	
	var label = Titanium.UI.createLabel({
		color:'#999',
		text:'I am a Basic Window',
		font:{fontSize:20,fontFamily:'Helvetica Neue'},
		textAlign:'center',
		width:'auto'
	});
	
	instance.add(label);
	
	return instance;
	
}

module.exports = BasicWindow;