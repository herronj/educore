
/**
 * General app styles in one place
 */
exports = {
	defaultWindow : {
		backgroundColor : '#ffffff',
		barColor: '#414444',
		fullscreen : false,
		navBarHidden : false
	},
	dashboard : {
		title : 'home',
		navBarHidden : true,
		exitOnClose : true,
		fullscreen : false
	},
	twitter : {
		backgroundColor : '#ffffff',
		barColor: '#414444',
		fullscreen : false
	},
	html : {
		backgroundColor: '#ffffff',
        navBarHidden: false,
        barColor: '#414444',
        width: '100%',
        height: '100%'
	}
};
