/**
 * Private methods
 */

/***************************
 * Public methods & variables
 ***************************/

/*
 * load app window
 * @return {object} instance
 */
function AppWindow(app, params) {
	
	// set module variables
	var iconHeight = 85,
	iconWidth = 102,
	imageSuffix = '',
	options;

	

	/*
	 * test for a large sreen
	 */
	if(app.isLargeScreen()) {
		iconHeight = 170;
		iconWidth = 204;
		imageSuffix = '@2x';
	}
	/*
	 * define main window icons/objects
	 */
	var icons = [{
		image : '/images/dashboard/rss' + imageSuffix + '.png',
		func : "RssWindow",
		params : {
			url : "http://www.uwosh.edu/today/category/campus-news/feed/",
			title : 'UW Oshkosh Today'
		}
	}, {
		image : '/images/dashboard/map' + imageSuffix + '.png',
		func : "BasicWindow"
	}, {
		image : '/images/dashboard/comment' + imageSuffix + '.png',
		func : "TwitterWindow"
	}];

	if(app.isLargeScreen()) {
		options = {
			mainBackgroundImage : 'images/home_ipad.png',
			dashboardHeight : 340,
			dashboardWidth : 612,
			mainBackgroundImageLandscape : 'images/home_ipad_l.png',
			dashboardHeightLandscape : 500,
			dashboardWidthLandscape : 410
		};
	} else if(app.isHighDensityScreen()) {
		options = {
			mainBackgroundImage : 'images/home@2x.png',
			dashboardHeight : 170,
			dashboardWidth : 306,
			mainBackgroundImageLandscape : 'images/home_l@2x.png',
			dashboardHeightLandscape : 260,
			dashboardWidthLandscape : 220
		};
	} else {
		options = {
			mainBackgroundImage : 'images/home.png',
			dashboardHeight : 170,
			dashboardWidth : 306,
			mainBackgroundImageLandscape : 'images/home_l.png',
			dashboardHeightLandscape : 260,
			dashboardWidthLandscape : 220
		};
	}

	var instance = Ti.UI.createWindow({
		title : 'home',
		navBarHidden : true,
		exitOnClose : true,
		backgroundImage: options.mainBackgroundImage
	});

	var dashboard = Ti.UI.createView({
		height : options.dashboardHeight,
		width : options.dashboardWidth,
		bottom : (app.isLargeScreen()) ? 150 : 75,
		borderRadius : 0,
		layout : 'horizontal'
	});

	for(var i = 0; i < icons.length; i++) {
		var view = Ti.UI.createView({
			backgroundImage : icons[i].image,
			top : 0,
			height : iconHeight,
			width : iconWidth
		});
		view.func = icons[i].func;
		view.params = icons[i].params;

		view.addEventListener('click', function(e) {
			app.loadPage(this.func,this.params);

		});
		dashboard.add(view);
	}

	Ti.Gesture.addEventListener('orientationchange', function(ev) {
		if(app.isLandscape(ev.orientation)) {
			// Update your UI for landscape orientation
			instance.backgroundImage = options.mainBackgroundImageLandscape;
			dashboard.height = options.dashboardHeightLandscape;
			dashboard.width = options.dashboardWidthLandscape;
			dashboard.left = (app.isLargeScreen()) ? 40 : 20;
			dashboard.bottom = (app.isLargeScreen()) ? 130 : 20;
		} else if(app.isPortrait(ev.orientation)) {
			// Update your UI for portrait orientation
			instance.backgroundImage = options.mainBackgroundImage;
			dashboard.height = options.dashboardHeight;
			dashboard.width = options.dashboardWidth;
			dashboard.left = (app.isLargeScreen()) ? 40 : 10;
			dashboard.bottom = (app.isLargeScreen()) ? 150 : 75;
		}
	});

	instance.add(dashboard);

	return instance;
}

module.exports = AppWindow;