
/**
 * Private methods
 */


/***************************
 * Public methods & variables
 ***************************/

/**
 * load a rss window
 * @param {string} args.title
 * @param {string} args.url
 */
function RssWindow(app, params) {
	
	//load dependacies
	var styles = require('ui/styles'),
		activityIndicator = require("ui/ActivityIndicator");
	
	var instance = Titanium.UI.createWindow({
		title : params.title,
		backgroundColor : '#ffffff',
		barColor: '#414444',
		fullscreen : false,
		navBarHidden : false
	});
	
	// startup activity indicator
	var ai = new activityIndicator();
	// add optional custom back button for the home view
	if (!app.isAndroid()) {
        var leftButton = Ti.UI.createButton({
            backgroundImage: 'images/6dots.png',
            width: 41,
            height: 30
        });
        leftButton.addEventListener('click', function () {
            app.closePage(instance, {
                animated: true
            });
        });
        instance.leftNavButton = leftButton;
    }
	var tableview = Titanium.UI.createTableView({
		backgroundColor : '#fff',
		layout : 'vertical'
	});
	instance.add(tableview);
	
	function createRss() {
		// create table view data object
		var data = [];
		
		ai.showModal('Loading RSS feed...', 6000, 'The RSS feed timed out.');
		
		var xhr = Ti.Network.createHTTPClient({
			onload: function() {
				try 
				{
					var doc = this.responseXML.documentElement;
					var items = doc.getElementsByTagName("item");
					var x = 0;
					var doctitle = doc.evaluate("//channel/title/text()").item(0).nodeValue;
					for(var c = 0; c < items.length; c++) {
						var item = items.item(c);
	
						var title = item.getElementsByTagName("title").item(0).text;
						var row = Ti.UI.createTableViewRow({
							height : 60,
							hasChild : true
						});
						var label = Ti.UI.createLabel({
							text : title,
							left : 5,
							top : 5,
							bottom : 5,
							right : 5,
							font : {
								fontSize : 14
							},
							color : '#000'
						});
						row.add(label);
						data[x++] = row;
						row.shortTitle = title;
						row.url = item.getElementsByTagName("link").item(0).text;
					}
					tableview.setData(data);
					
					ai.hideModal();
					
					tableview.addEventListener('click', function (e) {
						app.loadPage('HtmlWindow', { url:e.row.url, title:e.row.shortTitle});
					});
					
				} catch(E) {
					Titanium.API.log(E);
				}
			},
			onerror: function(e) {
				Ti.API.debug(e);
			},
			timeout: 5000
		});
		xhr.open("GET", params.url);

		xhr.send();
	}
	
	if(Ti.Network.online) {
			createRss();
	} else {
		alert('No network connection detected.');
	}
	
	return instance;

}

module.exports = RssWindow;
