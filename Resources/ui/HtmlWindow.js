

/***************************
 * Public methods & variables
 ***************************/

/**
 * builds an html web view
 * @param {object} app
 * @param {object} params
 */
function HtmlWindow(app, params) {
    var instance = Titanium.UI.createWindow({
		backgroundColor: '#ffffff',
        navBarHidden: false,
        barColor: '#414444',
        width: '100%',
        height: '100%',
        id: 'htmlWindow',
        title: params.title
    });
    var webview = Ti.UI.createWebView({
        url: params.url,
        width: '100%',
        height: '100%'
    });
    instance.add(webview);
    
    instance.addEventListener('focus', function (e) {
        webview.url = params.url;
        webview.height = '100%';
        webview.width = '100%';
    });
    
    /*
	 * the event handler for window closed is required to make sure 
	 * the navigation controller is serving the right page off the stack
	 */
	instance.addEventListener("close", function() {
		Ti.App.fireEvent("window.close");
	}); 
    
	return instance;
	
}

module.exports = HtmlWindow;