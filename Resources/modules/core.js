/**
 * App core functions
 */
var __isLargeScreen, 
	__isHighDensityScreen, 
	__isAndroid, 
	__isiPad, 
	properties   = {},
	plugins = {},
	analytics,
	empty = {},
	currentPage = null,
	windowStack = [],
	navGroup;


/***************************
 * Public methods & variables 
 ***************************/

/**
 * Set properties for app
 * @param {String} name Name of property
 * @param {String} value Value of property	
 */
exports.addProperty = function(name, value) {
	properties[name] = value;
};

/**
 * Register a plugin with the core app
 * @param {String} name Name of plugin
 * @param {Object} object The object namespace the plugin uses
 */	
exports.register = function(name, object) {
	plugins[name] = object;
};


/**
 * Helper method to show all properties in the core app
 */
exports.properties = function() { 
	return properties; 
};

/**
 * Helper method to show one property in the core app
 * @param {String} name
 */
exports.property = function(name) { 
	return properties[name]; 
};

/**
 * Helper method to return windowStack
 * @return {Array}
 */
exports.getWindowStack = function() {
  return windowStack;
}

/**
 * test to see if this is a tablet
 */	
exports.isLargeScreen = function() {
	if (__isLargeScreen === undefined) {
		__isLargeScreen = (Ti.Platform.displayCaps.platformWidth >= 700);
	}
	return __isLargeScreen;
};
/**
 * test for high density
 */
exports.isHighDensityScreen = function() {
	if (__isHighDensityScreen === undefined) {
		__isHighDensityScreen = (Ti.Platform.displayCaps.density === 'high');
	}
	return __isHighDensityScreen;
};
	
/**
 * test for android
 */
exports.isAndroid = function() {
	if (__isAndroid === undefined) {
		__isAndroid = (Ti.Platform.osname === 'android');
	}
	return __isAndroid;
};

/**
 * test for iPad
 */
exports.isiPad = function() {
	if (__isiPad === undefined) {
		__isiPad = (Ti.Platform.osname == 'ipad');
	}
	return __isiPad;
};

/**
 * test for landscpae orientation
 */
exports.isLandscape = function (orient) {
	orient = orient || Ti.UI.orientation;
	return orient == Ti.UI.LANDSCAPE_LEFT || orient == Ti.UI.LANDSCAPE_RIGHT;
};
 
/**
 * test for a portrait orientation
 */
exports.isPortrait = function (orient) {
	orient = orient || Ti.UI.orientation;
	return orient == Ti.UI.PORTRAIT || orient == Ti.UI.UPSIDE_PORTRAIT;
};

/**
 * converts special characters into nice chars for display
 * @param {string} str
 */
exports.cleanSpecialChars = function(str) {
	if (str === null) {
		return '';
	}
	if (typeof str === 'string') {
		return  str
		.replace(/&quot;/g,'"')
		.replace(/\&amp\;/g,"&")
		.replace(/&lt;/g,"<")
		.replace(/&gt;/g,">")
		.replace(/&#039;/g, "'");
	}
	return '';
};

/***************************
 * Implements a Navigation controller
 ***************************/

/**
 * loads a new page and on iOS builds a NavGroup
 * @param {string} name
 * @param {object} params
 */
exports.loadPage = function(name, params) {
	
	currentPage = require(properties.pages + '/' + name);
	
	windowStack.push(currentPage);
	
	var instance = currentPage(exports, params);
	
	//This is the first window
	if(windowStack.length === 1) {
		if(exports.isAndroid()) {
			instance.exitOnClose = true;
			instance.open({animate: true});
		} else {
			navGroup = Ti.UI.iPhone.createNavigationGroup({
				window : instance
			});
			var containerWindow = Ti.UI.createWindow();
			containerWindow.add(navGroup);
			containerWindow.open();
		}
	}
	//All subsequent windows
	else {
		if(exports.isAndroid()) {
			instance.open({animate: true});
		} else {
			navGroup.open(instance);
		}
	}
	
};

exports.closePage = function(windowToClose) {
	// the close action chould hit the close event handler 
	//and pop it off the stack
	navGroup.close(windowToClose);
};

exports.goHome = function() {
	//store a copy of all the current windows on the stack
	var windows = windowStack.concat([]);
	for(var i = 1, l = windows.length; i < l; i++) {
		if (navGroup) { navGroup.close(windows[i]); } else { windows[i].close(); }
	}
	windowStack = [windowStack[0]]; //reset stack
};





